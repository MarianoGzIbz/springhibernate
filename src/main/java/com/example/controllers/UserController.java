package com.example.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.entities.Users;
import com.example.services.UsersService;

@Controller
@RequestMapping(value = "users")
public class UserController {

	@Autowired
	UsersService userService;

	@RequestMapping(value = "page", method = RequestMethod.GET)
	public ModelAndView name() {
		ModelAndView view = new ModelAndView("hello");
		return view;
	}

	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved(Users users) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (userService.saveOrUpdate(users)) {
			map.put("status", "200");
			map.put("message", "record saved correctly");
		}

		return null;
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Users users) {
		Map<String, Object> map = new HashMap<String, Object>();

		List<Users> userList = userService.list();

		if (userList != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", userList);
		} else {
			map.put("status", "404");
			map.put("message", "there are not records");
		}

		return map;
	}
}
